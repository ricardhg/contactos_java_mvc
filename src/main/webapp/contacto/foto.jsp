<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>

<!-- 
    IMPORTANTE: hay que añadir a pom.xml la dependencia:
     <dependency>
      <groupId>commons-fileupload</groupId>
      <artifactId>commons-fileupload</artifactId>
      <version>1.3</version>
    </dependency>
    además, es necesario importar las siguientes clases:
    -->

<%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>



<%
    //debemos recibir el id del contacto al cual vamos a cambiar la foto,
    //si no volvemos a index
    String idcontacto_s = request.getParameter("id");

    int idcontacto = 0;
    if (idcontacto_s == null) {
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect( request.getContextPath() +"/index.jsp");
        return;
    } else {
         idcontacto = Integer.parseInt(idcontacto_s);
         //igualmente, si id_numerico no es válido (>0), nos vamos
         if (idcontacto<=0) {
            response.sendRedirect( request.getContextPath() +"/index.jsp");
            return;   
         }
    }

    // solo llegamos aquí si tenemos un idcontacto válido! seguimos...
    Contacto cn = ContactoController.getContactById(idcontacto);
    
    // UPLOAD DE IMAGEN
    // creamos archivo y establecemos límites de espacio

    File file ;
    int maxFileSize = 10000 * 1024; // 10 mb tamaño máximo en disco
    int maxMemSize = 5000 * 1024; // 5 mb tamaño maximo en memoria
    
    boolean muestraForm = true;

    String pathImagenes = "/uploads"; // carpeta donde guardaremos las imágenes
    //String urlBase = request.getContextPath() + pathImagenes;

    // archivoDestino contendrá el nombre del archivo que crearmos
    String archivoDestino = null;
    String pieFoto = null;

    //getRealPath nos da la ubicación en disco de la carpeta /uploads necesaria para guardar el archivo subido
    //podria ser cualquier carpeta con acceso de escritura para el usuario "tomcat" en linux
    //o cualquier carpeta no protegida de windows
    //en cualquier caso, las "/" son estilo linux, ex: "c:/usuarios/nombre/uploads"
    //en este caso suponemos que es la carpeta uploads dentro de webapp

    // IMPORTANTE añadir el "/" al final!

    String realPath = pageContext.getServletContext().getRealPath(pathImagenes)+"/";
    

    // verificamos si estamos recibiendo imagen
    // en dos sentidos: si es POST y si es tipo multipart
    String contentType = request.getContentType();
    if ("POST".equalsIgnoreCase(request.getMethod()) && contentType.indexOf("multipart/form-data") >= 0) {

      DiskFileItemFactory factory = new DiskFileItemFactory();
      factory.setSizeThreshold(maxMemSize);
      //por si hiciese falta, indicamos la carpeta tmp
      factory.setRepository(new File(System.getProperty( "java.io.tmpdir" )));
      ServletFileUpload upload = new ServletFileUpload(factory);
      upload.setSizeMax( maxFileSize );
      try{ 
        // ojo, fileItems no sólo incluye archivos, podrian ser también
        // campos INPUT tales como el pie de foto
         List<FileItem> formItems = upload.parseRequest(request);
       
        if (formItems != null && formItems.size() > 0) {
            
            for (FileItem fi : formItems) {
                if ( fi.isFormField() )  {
                    // hemos recibido un campo de formulario, NO FILE
                    // pero no podemos utilizar getParameter...
                    // podemos utilizar un switch...case... para verificar el "fieldName"
                    String fieldname = fi.getFieldName();
                    String fieldvalue = fi.getString();
                    //en este caso concreto sabemos que puede ser el pie
                    if (fieldname.equals("pie")){
                        pieFoto = fieldvalue;
                    }
                }else{

                    //si no es formfiueld es un FILE, obtenemos tamaño
                    if (fi.getSize()>0){
                        //filename es el nombre del archivo que se ha subido
                        String fileName = fi.getName();
                                
                        // valores disponibles, que no utilizamos: 
                        //   String fieldName = fi.getFieldName();
                        //   boolean isInMemory = fi.isInMemory();
                        //   long sizeInBytes = fi.getSize();
                        
                        // creamos puntero a archivo ubicado en carpeta real del disco
                        // más nombre de archivoDestino (igual al que se ha subido en este caso)
                        // IMPORTANTE: podríamos cambiar aquí el nombre de archivo destino
                        archivoDestino = fileName;
                        file = new File( realPath + archivoDestino ); 
                        
                        //qué pasa si archivo ya existe? nos inventamos un prefijo
                        //foto.jpg, 1_foto.jpg, 2_foto.jpg...
                        int t = 0;
                        String archivoTemp = archivoDestino;
                        while (file.exists() && !file.isDirectory()){
                            archivoTemp = String.valueOf(++t)+"_"+archivoDestino;
                            file = new File( realPath + archivoTemp ); 
                        }
                        archivoDestino=archivoTemp;

                        // escribimos archivo en disco:
                        fi.write( file ) ;

                    }else{
                        System.out.println("NO File!");
                    }
                } 

      
            }
          }

      }catch(Exception ex) {
          //error, mostramos mensaje y redirigimos a listado...
         System.out.println(ex);
       

      }


        //importante, es posible que tanto archivoDestino como pieFoto sean null, 
        //ContactoController deberá gestionarlo...
        ContactoController.setUrlfoto(idcontacto, archivoDestino, pieFoto);

        //volvemos a listado...
        response.sendRedirect( request.getContextPath() +"/contacto/list.jsp");
         return;

    }


       %>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>AcademiaApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">
</head>
<body>

    <%@include file="/menu.jsp"%>

    <div class="container">
    <div class="row">
    <div class="col">
    <h2>Foto para <%= cn.getNombre() %></h2>
    </div>
    </div>

    <div class="row">
    <div class="col-md-4">
    <img class="img-thumbnail" src="<%= cn.getUrlfotoPath() %>" />
    </div>
    </div>


    <div class="row">

   
    <div class="col-md-8">

    <form id="formcrea" action="#" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <label for="foto">Fotografia</label>
        <input type="file" class="form-control-file" name="foto" id="foto">
    </div>
 
    <div class="form-group">
        <label for="pie">Pie de foto</label>
        <input type="text" class="form-control-file" name="pie" id="pie" value="<%= cn.getPiefoto() %>">
    </div>

        <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
    </div>
 
    </div>
    </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>


</body>
</html>
