<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>


<%
    //debemos recibir el id del contacto al cual vamos a cambiar la foto,
    //si no no vamos bien...
    String idcontacto_s = request.getParameter("id");
    int idcontacto = 0;
    if (idcontacto_s == null) {
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect( request.getContextPath() +"/index.jsp");
        return;
    } else {
         idcontacto = Integer.parseInt(idcontacto_s);
         //igualmente, si id_numerico no es válido (>0), nos vamos
         if (idcontacto<=0) {
            response.sendRedirect( request.getContextPath() +"/index.jsp");
            return;   
         }
    }

    // solo llegamos aquí si tenemos un idcontacto válido! seguimos...
    Contacto cn = ContactoController.getContactById(idcontacto);
    
       %>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>AcademiaApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">
</head>
<body>

    <%@include file="/menu.jsp"%>

    <div class="container">
    <div class="row">
    <div class="col">
    <h2>Foto para <%= cn.getNombre() %></h2>
    </div>
    </div>

    <div class="row">
    <div class="col-md-4">
    <img class="img-thumbnail" src="<%= cn.getUrlfotoPath() %>" />
    </div>
    </div>


    <div class="row">


    <div class="col-md-8">

    <form id="formcrea" action="/contactos/subefoto" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <label for="foto">Fotografia</label>
        <input type="file" class="form-control-file" name="foto" id="foto">
    </div>
 
    <div class="form-group">
        <label for="pie">Pie de foto</label>
        <input type="text" class="form-control-file" name="pie" id="pie" value="<%= cn.getPiefoto() %>">
    </div>
    
        <input type="hidden" name="id" value="<%= idcontacto %>">

        <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
    </div>

    </div>
    </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>


</body>
</html>
