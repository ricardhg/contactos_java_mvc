package com.enfocat.mvc;

import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;


import com.mysql.jdbc.Connection;



public class DBDatos {
    
    private static final String TABLE = "contactos";
    private static final String KEY = "id"; 
   
    public static Contacto newContacto(Contacto cn){
            // NO!!!! String sql =  "INSERT INTO contactos (nombre, email) VALUES ('"+cn.getNombre()+"','"+cn.getEmail()+"')";
            String sql =  "INSERT INTO contactos (nombre, email) VALUES (?,?)";
           
            try (Connection conn = DBConn.getConn();
                    PreparedStatement pstmt = conn.prepareStatement(sql);
                    Statement stmt = conn.createStatement()) {
                
                pstmt.setString(1, cn.getNombre());
                pstmt.setString(2, cn.getEmail());
                pstmt.executeUpdate(); 
            
                //usuario nuevo, actualizamos el ID con el recién insertado
                ResultSet rs = stmt.executeQuery("select last_insert_id()"); 
                if (rs.next()) { 
                    cn.setId(rs.getInt(1));
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            return cn;
    }


  
    public static Contacto getContactoId(int id){
        Contacto cn = null;
        String sql = String.format("select %s,nombre,email from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                cn = new Contacto(
                (Integer) rs.getObject(1),
                (String) rs.getObject(2),
                (String) rs.getObject(3)
                );
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return cn;
    }
    




    public static boolean updateContacto(Contacto cn){
        boolean updated = false;
        String sql = String.format("UPDATE contactos set nombre=?, email=? where id=%d", cn.getId());
    
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            
            pstmt.setString(1, cn.getNombre());
            pstmt.setString(2, cn.getEmail());
            pstmt.executeUpdate(); 
            updated = true;
        
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return updated;
    }


    public static boolean deleteContactoId(int id){
        boolean deleted = false;
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
            deleted=true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return deleted;
    }

    


    public static List<Contacto> getContactos() {
        List<Contacto> listaContactos = new ArrayList<Contacto>();  
        String sql = "select id,nombre,email from contactos"; 
        try (Connection conn = DBConn.getConn();
            Statement stmt = conn.createStatement()) { 
                /*
                  Alumno u = new Alumno(
        rs.getInt(KEY),
        rs.getString("nombre"),
        rs.getString("email"),
        rs.getString("telefono"),
        rs.getDate("alta")
    );

    */
                ResultSet rs = stmt.executeQuery(sql);  
                while (rs.next()) {
                    Contacto u = new Contacto(
                        rs.getInt("id"),
                        rs.getString("nombre"),
                        rs.getString("email")); 
                        listaContactos.add(u);
                    }
        
        } catch (Exception e) { 
        String s = e.getMessage();
        System.out.println(s);
        }
        return listaContactos; 
    }



}

